<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Article</title>
</head>
<body>
<h1>Add Article</h1>

{!! Form::open(array('action' => 'ArticleController@store', 'id' => 'createarticle')) !!}
{{ csrf_token() }}
<div class="row large-12 columns">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns">
    {!! Form::label('content', 'Detail:') !!}
    {!! Form::textarea('content', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'large-8 columns']) !!}
</div>
<!-- Using [] has turned the variable into an array.-->
<div class="row large-12 columns">
    {!! Form::label('category', 'Category:') !!}
    {!! Form::select('category[]', $cats, null,['class' => 'large-8 columns', 'multiple']) !!}
</div>


<div class="row large-4 columns">
    {!! Form::submit('Add Article', ['class' => 'button']) !!}
</div>
{!! Form::close() !!}

</body>
</html>